# Otus_task11

SELinux - когда все запрещено. Разбираемся, что такое SELinux


Столкнулся с SE Linux при установке Zabbix server-а


Обновил php, поставил базу, выгрузил в нее схему, httpd готов к работе. 
Устанавливаю сервер, вебморду, клиент. Стартую сервер - не стартует `Active: inactive (dead)`

```
сен 20 13:07:45 server systemd[1]: zabbix-server.service: control process exited, code=exited status=1
сен 20 13:07:45 server systemd[1]: Unit zabbix-server.service entered failed state.
сен 20 13:07:45 server systemd[1]: zabbix-server.service failed.
```

а в messages
```
Can't open PID file /run/zabbix/zabbix_server.pid (yet?) after start: No such file or directory
server setroubleshoot: SELinux is preventing /usr/sbin/zabbix_server_mysql from create access on the sock_file zabbix_server_preprocessing.sock. 
For complete SELinux messages run: sealert -l 588be3aa-6133-467b-a403-cb0b1d5ae9c2
```

yum install -y policycoreutils-python setroubleshoot setools &/& sealert -l 588be3aa-6133-467b-a403-cb0b1d5ae9c2

```
Если вы считаете, что zabbix_server_mysql должно быть разрешено create доступ к zabbix_server_alerter.sock sock_file по умолчанию.
То рекомендуется создать отчет об ошибке.
Чтобы разрешить доступ, можно создать локальный модуль политики.
Сделать
allow this access for now by executing:
# ausearch -c 'zabbix_server' --raw | audit2allow -M my-zabbixserver
# semodule -i my-zabbixserver.pp

Дополнительные сведения:
Исходный контекст             system_u:system_r:zabbix_t:s0
Целевой контекст              system_u:object_r:zabbix_var_run_t:s0
Целевые объекты               zabbix_server_alerter.sock [ sock_file ]
Источник                      zabbix_server
Путь к источнику              /usr/sbin/zabbix_server_mysql
Порт                          <Unknown>
Узел                          server
Исходные пакеты RPM           zabbix-server-mysql-4.2.6-1.el7.x86_64
Целевые пакеты RPM
Пакет регламента              selinux-policy-3.13.1-252.el7.1.noarch
SELinux активен               True
Тип регламента                targeted
Режим                         Enforcing
Имя узла                      server
Платформа                     Linux server 3.10.0-957.12.2.el7.x86_64 #1 SMP Tue
                              May 14 21:24:32 UTC 2019 x86_64 x86_64
Счетчик уведомлений           250
Впервые обнаружено            2019-09-20 12:46:04 UTC
В последний раз               2019-09-20 13:25:25 UTC
Локальный ID                  588be3aa-6133-467b-a403-cb0b1d5ae9c2

Построчный вывод сообщений аудита
type=AVC msg=audit(1568985925.555:3932): avc:  denied  { create } for  pid=28734 comm="zabbix_server" name="zabbix_server_alerter.sock" 
scontext=system_u:system_r:zabbix_t:s0 tcontext=system_u:object_r:zabbix_var_run_t:s0 tclass=sock_file permissive=0
```
Что ж применяем 
ausearch -c 'zabbix_server' --raw | audit2allow -M my-zabbixserver
semodule -i my-zabbixserver.pp

и работаем? 
что то не взлетело, попробуем еще но другой инcтрукции
```
semodule -l | grep zabbix - смотрим какие модули установлены


  205  semodule -r my-zabbixserver - удаляем наш модуль
  206  semodule -R - перезагружаем
  207  : > /var/log/audit/audit.log - чистим лог
  210  systemctl start zabbix-server - стартуем
  211  grep ^type=AVC /var/log/audit/audit.log > zabbix.log
  213  audit2why < zabbix.log
type=AVC msg=audit(1568988299.437:5034): avc:  denied  { create } for  pid=9626 comm="zabbix_server" name="zabbix_server_alerter.sock" 
scontext=system_u:system_r:zabbix_t:s0 tcontext=system_u:object_r:zabbix_var_run_t:s0 tclass=sock_file permissive=0
	Was caused by:
		Missing type enforcement (TE) allow rule.
		You can use audit2allow to generate a loadable module to allow this access.
        
  214  audit2allow -m zabbix < zabbix.log > zabbix.te
  215  checkmodule -M -m -o zabbix.mod zabbix.te
  216  semodule_package -o zabbix.pp -m zabbix.mod - Делаем свой модуль политики
  217  semodule -i zabbix.pp - не ставится, дефолтный так же называется
  218  mv zabbix.pp zabbix_add.pp
  219  semodule -i zabbix_add.pp -ставим 
  220  semodule -R - перезагружаемся
  221  systemctl status zabbix-server -l - наконец то 

```
нет, он падает и не поднимается. Делаю еще одну попытку перед тем как сделать `semanage permissive -a zabbix_t`
Удаляю старый модуль. И запускаем команду `audit2allow -a -m zabbix_new`

```
cat zabbix_old.te -- старый формировавшийся из лога

module zabbix_old 1.0;

require {
	type zabbix_var_run_t;
	type zabbix_t;
	class sock_file create;
}
#============= zabbix_t ==============
allow zabbix_t zabbix_var_run_t:sock_file create;
-------------------------------------------------

$cat zabbix_new.te - новый который только что сформировали аудитом, разрешаем все что просит

module zabbix_new 1.0;

require {
	type zabbix_var_run_t;
	type zabbix_t;
	class sock_file { create unlink };
	class unix_stream_socket connectto;
}
#============= zabbix_t ==============
#!!!! The file '/run/zabbix/zabbix_server_lld.sock' is mislabeled on your system.
#!!!! Fix with $ restorecon -R -v /run/zabbix/zabbix_server_lld.sock
#!!!! This avc can be allowed using the boolean 'daemons_enable_cluster_mode'
allow zabbix_t self:unix_stream_socket connectto;
allow zabbix_t zabbix_var_run_t:sock_file { create unlink };


```
Разница меня удивила, с этим подулем политики, заработал сервер Active: active (running) и selinux работает


Настроил веб-морду, она говорит нет подключения к заббикс серверу, делаем вот так
```
getsebool -a | grep zabbix
httpd_can_connect_zabbix --> off
zabbix_can_network --> off
zabbix_run_sudo --> off
[root@server ~]# setsebool -P httpd_can_connect_zabbix=on  (-P - persistent)
```

Подключение к заббиксу есть, соответственно можно сразу и остальные включить опции



